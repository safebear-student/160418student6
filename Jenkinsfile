pipeline {
    agent any
    //which server will be running the pipeline
    parameters{
        //Tests
        string(name: 'apiTests', defaultValue: 'ApiTestsIT', description: 'API tests')
        string(name: 'cuke', defaultValue: 'RunCukesIT', description: 'cucumber tests')
        //Test environment
        string(name: 'context', defaultValue: 'safebear', description: 'application context')
        string(name: 'domain', defaultValue: 'http://54.191.135.203', description: 'domain of the test environment')
        //Tomcat parameters
        string(name: 'test_hostname', defaultValue: '54.191.135.203', description: 'hostname of the test environment')
        string(name: 'test_port', defaultValue: '8888', description: 'port of the test env')
        string(name: 'test_username', defaultValue: 'tomcat', description: 'username of tomcat')
        string(name: 'test_password', defaultValue: 'tomcat', description: 'password of tomcat server')
        //Headless mode
        string(name: 'browser', defaultValue: 'headless', description: 'browser for our gui tests')


    }

    options {
        // allows us to keep only the build artifacts and build logs of the last 3 builds
        buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3'))
    }

    triggers {

        pollSCM('H/1 * * * *') // poll the source code repo every minute
    }

    stages {

        stage('Build with Unit testing') {
            /* run the mvn package command to ensure build the app and run the unit */
            steps {
                sh 'mvn clean package'
            }
            post {
                /* only run if the last build succeeds*/
                success {
                    // print message to screen that we are archiving
                    echo 'Now archiving...'
                    //archive the artifacts so we can build once and and then use them later to deploy
                    archiveArtifacts artifacts: '**/target/*.war'
                }
                always {
                    junit "**/target/surefire-reports/*.xml"
                }
            }
        }
        stage('Static Analysis'){
            /* run the mvn checkstyle:checkstyle command to run the static analysis, Can be done in parallel to the build and unit testing step. */
            steps {
                sh 'mvn checkstyle:checkstyle'
            }
            post {
                    success {
                        checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''
                    }
            }
        }

        stage('Deploy to Test'){
            steps{
                sh 'mvn cargo:redeploy -Dcargo.hostname=${test_hostname} -Dcargo.servlet.port=${test_port} -Dcargo.username=${test_username} -Dcargo.password=${test_password}'
            }
        }

        stage('Integration Tests'){
            steps{
                sh 'mvn clean -Dtest=${apiTests} test -Ddomain=${domain} -Dport=${test_port} -Dcontext=${context}'

            }
            post{
                always{
                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
                }
            }
        }

        stage('BDD Tests'){
            steps {
                sh 'mvn clean -Dtest=${cuke} verify -Ddomain=${domain} -Dport=${test_port} -Dcontext=${context} -Dbrowser=${browser}'
            }
            post {
                always{
                    publishHTML([
                            allowMissing: false,
                            alwaysLinkToLastBuild: false,
                            keepAll: false,
                            reportDir: 'target/cucumber',
                            reportFiles: 'index.html',
                            reportName: 'BDD Report',
                            reportTitles: ''

                    ])
                }

            }
        }




    }
}
