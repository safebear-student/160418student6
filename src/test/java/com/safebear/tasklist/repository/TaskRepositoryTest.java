package com.safebear.tasklist.repository;

import com.safebear.tasklist.model.Task;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;


/**
 * Created by CCA_Student on 17/04/2018.
 */

@DataJpaTest
@RunWith(SpringRunner.class)

public class TaskRepositoryTest {
        @Autowired
        private TaskRepository repo;

        @Test
        public void findByTaskName() {
            LocalDate dueDate = LocalDate.now();
            this.repo.save(new Task(null, "Configure Jenkins", dueDate, false));


            List<Task> taskList = this.repo.findByName("Configure Jenkins");
            Assertions.assertThat(taskList.size()).isEqualTo(1);

        }
}
