package com.safebear.tasklist;

import com.safebear.tasklist.model.Task;

import java.time.LocalDate;

/**
 * Created by CCA_Student on 17/04/2018.
 */
public class BaseUnitTest {

        LocalDate dueDate = LocalDate.now();
        protected Task task = new Task(1L, "Configure Jenkins", dueDate, false);
    }
