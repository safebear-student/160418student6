package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import java.time.LocalDate;

/**
 * Created by CCA_Student on 17/04/2018.
 */
public class TaskTest {
    @Test
    public void creation(){
        LocalDate dueDate = LocalDate.now();

        Task task = new Task(1L, "Configure Jenkins server", dueDate, false);

        Assertions.assertThat(task.getId()).isEqualTo(1L);
        Assertions.assertThat(task.getName()).isNotBlank();
        Assertions.assertThat(task.getName()).isEqualTo("Configure Jenkins server");
        Assertions.assertThat(task.getDueDate()).isEqualTo(dueDate);
        Assertions.assertThat(task.getCompleted()).isEqualTo(false);


    }
}
