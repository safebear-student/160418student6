package com.safebear.tasklist.repository;

import com.safebear.tasklist.model.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by CCA_Student on 17/04/2018.
 */
public interface TaskRepository extends CrudRepository<Task, Long> {
    List<Task> findByName(String taskname);
}
