package com.safebear.tasklist.service;

import com.safebear.tasklist.model.Task;

/**
 * Created by CCA_Student on 17/04/2018.
 */
public interface TaskService {

    Iterable<Task> list();

    Task save(Task task);
}
